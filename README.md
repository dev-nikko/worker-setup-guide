# Worker Setup Guide #

### How do I get set up? ###

1. Clone worker [repo](https://bitbucket.org/gaapapps/gappify-v1-worker/)
2. Checkout to prod branch
3. Remove mongodb dependency from composer.json
4. Run `composer update`
5. Create `.env` and `.env.dusk.local` to the root of the repo
6. Copy the contents of [`.env.example`](.env.example) and paste to your `.env` and `.env.dusk.local`
7. Setup database connections for `.env` and `.env.dusk.local`
    ```
    MAIN_DB_DATABASE=
    MAIN_DB_USERNAME=root
    MAIN_DB_PASSWORD=

    DB80_DB_DATABASE=
    DB80_DB_USERNAME=root
    DB80_DB_PASSWORD=

    DB118_DB_DATABASE=
    DB118_DB_USERNAME=root
    DB118_DB_PASSWORD=
    ```
8. Run `php artisan key:generate`
9. Copy `APP_KEY` from `.env` to `.env.dusk.local`
10. Setup `APP_URL` for `.env` and `.env.dusk.local`
11. Setup `API_KEY` & `API_URI` for `.env` and `.env.dusk.local`
    ```
    // Example
    API_KEY=6bIwYrTIX4EyX5fXqaeu99SWhWLCQl1Y
    API_URI="https://vendor-accruals.test/
    ```
12. Setup AWS keys for `.env` and `.env.dusk.local`
    ```
    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=
    AWS_BUCKET=
    ```
13. Setup TESTs keys for `.env` and `.env.dusk.local`
    ```
    TEST_TRIGGER_URL=true
    TEST_CONNECTION=
    TEST_TOTAL_CSV_ROWS=7
    TEST_EMAIL=
    ```
14. Setup database (*skip this if you already cloned and setup [gappify-v1](https://bitbucket.org/gaapapps/gappify-v1/)*)
    1.  Create a "global_uat" schema in your localhost.
    2.  Download [gappify-v1 database dump.gz.](https://drive.google.com/file/d/1B2wTgxFVAgtsH3Jy8e04DjbKXBRP7tIb/view?usp=sharing)
    3.  import `gappify-v1 database` sql dump
15. Download and import [gpfy-db-qa.sql](https://drive.google.com/file/d/1oDJgs9daMo3ZmgVWuu2MqYNFUhbwdJIc/view)
16. Set up `gaap_app_integrations` table
    1.  Open `gaap_app_integrations` table on database based on the value that is set `TEST_CONNECTION` env variable
    2.  Update the `inbound_keyword` field of `gaap_app_integrations`. Add suffix to each record *(e.g. `_your_name`)*
17. Run `php artisan dusk --group=inbound --testdox` to test your worker setup
18. Run `php artisan dusk --filter=GenerateTransactionsManuallyTest --testdox` to create transaction

### Contribution guidelines ###

* Should I miss any, feel free to submit a pull request.

### Who do I talk to? ###

* Repo owner or admin
* PH Dev Team group chat

----
#### Footnotes: ###
* Initial email (newly created transaction)
* On browser, go to [https://{your-web-domain}/run-queue-jobs?key=lljwBl9WzpD6pcapsYh4jnx3hG1gcW](on browser, go to https://{your-web-domain}/run-queue-jobs?key=lljwBl9WzpD6pcapsYh4jnx3hG1gcW)
* To get AWS Key ID and Secret Access Key, check this [guide](AwsKeysGuide.md).