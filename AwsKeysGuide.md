# AWS Keys Guide #

### How do I get my AWS unique keys? ###

1. Log in to your AWS Console account
2. Go to ***Security credentials*** page *(located on the upper right corner on the page under on your account dropdown menu)*
3. Under the *AWS IAM credentials* tab, click **Create access key**
4. To see the new access key, choose **Show**. Your credentials resemble the following:
      * Access key ID: AKIAIOSFODNN7EXAMPLE
      * Secret access key: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
5. To download the key pair, choose **Download .csv file**. Store the .csv file with keys in a secure location.